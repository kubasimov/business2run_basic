using System.Collections.Generic;

namespace Model
{
    public class MUser
    {
        public MUser()
        {
            
        }
        public MUser(string name, string surname)
        {
            Name = name;
            Surname = surname;
            
        }
        
        public string Name { get; set; }
        public string Surname { get; set; }
        public MAdress MAdress { get; set; }
        public ICollection<string> Emails { get; set; }
    }
}