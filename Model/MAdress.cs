namespace Model
{
    public class MAdress
    {
        public MAdress(string state, string city, string street, string house, string place)
        {
            State = state;
            City = city;
            Street = street;
            House = house;
            Place = place;
        }

        public MAdress()
        {}

        public string State { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Place { get; set; }
    }
}