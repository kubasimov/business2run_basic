﻿using System.Collections.Generic;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Xunit;
using Xunit;
using Xunit.Extensions;

namespace Model.Tests
{
    public class MUserTests
    {
        private readonly Fixture _fixture;

        public MUserTests()
        {
            _fixture = new Fixture();
        }

        [Fact]
        public void New_Blank_MUser()
        {
            var user = new MUser();

            Assert.NotNull(user);
        }

        [Theory]
        [InlineAutoData]
        [InlineAutoData("")]
        [InlineAutoData("","")]
        public void New_MUser_With_Name_Surname(string name,string surname)
        {
            var user = new MUser(name, surname);

            Assert.Equal(name,user.Name);
            Assert.Equal(surname,user.Surname);
        }

        [Theory,AutoData]
        public void New_MUser_from_Anoder_User(string name, string surname)
        {
            var user = new MUser(name, surname);
            var actual = user;

            Assert.Equal(user,actual);
        }

        [Theory,AutoData]
        public void Add_Adress_to_Exiting_MUser(string name,string surname)
        {
            var user = new MUser(name, surname);
            var adress = new MAdress();

            user.MAdress = adress;

            Assert.Equal(adress,user.MAdress);
        }

        [Theory, AutoData]
        public void Add_Email_List_to_Exiting_MUser(string name, string surname)
        {
            var user = new MUser(name, surname);
            var emails = _fixture.Create <ICollection<string>>();

            user.Emails = emails;

            Assert.Equal(emails,user.Emails);
        }
    }
}
