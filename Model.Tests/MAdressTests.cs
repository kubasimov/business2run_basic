﻿using Ploeh.AutoFixture.Xunit;
using Xunit;
using Xunit.Extensions;

namespace Model.Tests
{
    public class MAdressTests
    {
        [Fact]
        public void New_Blank_MAdress()
        {
            var adress = new MAdress();

            Assert.NotNull(adress);
        }

        [Theory]
        [InlineAutoData("")]
        [InlineAutoData("","")]
        [InlineAutoData("","","")]
        [InlineAutoData("","","","")]
        [InlineAutoData("","","","","")]
        [InlineAutoData]
        public void New_MAdress_with_Full_Adress(string state, string city, string street, string house, string place )
        {
            var adress = new MAdress(state, city, street, house, place);

            Assert.Equal(state,adress.State);
            Assert.Equal(city,adress.City);
            Assert.Equal(street,adress.Street);
            Assert.Equal(house,adress.House);
            Assert.Equal(place,adress.Place);
        }
    }
}
