﻿using System;
using System.Globalization;
using Ploeh.AutoFixture;
using Ploeh.AutoFixture.Xunit;
using Xunit;
using Xunit.Extensions;

namespace Model.Tests
{
    public class MOrderTests
    {
        [Fact]
        public void New_Blank_Order()
        {
            var order = new MOrder();

            Assert.NotNull(order);
        }

        [Fact]
        public void Add_User_To_Exiting_Order()
        {
            var order = new MOrder();
            var user = new MUser();

            order.User = user;

            Assert.Equal(user, order.User);
        }

        
        [Theory]
        [InlineAutoData]
        [InlineAutoData]
        public void Add_OrderNumber_To_Exiting_Order(string number)
        {
            var order = new MOrder();
            
            order.Number = number;

            Assert.Equal(number,order.Number);
        }
    }
}